FROM nginx:alpine
MAINTAINER Christopher Ostrouchov "chris.ostrouchov@gmail.com"

COPY build/es5-bundled/ /usr/share/nginx/html/

EXPOSE 80
