{
    "title": "Hello World Example",
    "author": "Chris Ostrouchov",
    "date": "2016-12-04",
	"image": "/images/blog/paul-gilmore.jpg",
    "tags": ["test", "hello world"]
}

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
eiusmod tempor incididunt ut labore et dolore magna aliqua. Ultrices
sagittis orci a scelerisque purus semper eget. Ut morbi tincidunt
augue interdum velit.

Duis ut diam quam nulla porttitor massa. Mattis molestie a iaculis at
erat pellentesque adipiscing commodo elit. Tincidunt ornare massa eget
egestas purus viverra accumsan. Diam vel quam elementum pulvinar. Diam
vel quam elementum pulvinar. Mauris pellentesque pulvinar pellentesque
habitant. Est ultricies integer quis auctor elit. Sem nulla pharetra
diam sit amet nisl suscipit.


1. Item 1
2. Item 2
3. Item 3

Additional Items:

1. Item 1
2. Item 2
3. Item 3
4. Item 4 ...

- Item 1
- Read 2
- Do first step 3

| Size  | Material     | Color        |
| ----: | :----------- | :----------- |
| 9     | leather      | brown        |
| 10    | hemp canvas  | natural      |
| 11    | glass        | transparent  |

Lorium ipsum this is some great text \(\frac{\beta}{\gamma}\) with
some more text.  Then we add some display text. Some more stuff to
talk about.  is simply dummy text of the printing and typesetting
industry. Lorem Ipsum has been the industry's standard dummy text ever
since the 1500s, when an unknown printer took a galley of type and
scrambled it to make a type specimen book. It has survived not only
five centuries, but also the leap into electronic typesetting,
remaining essentially unchanged. It was popularised in the 1960s with
the release of Letraset sheets containing Lorem Ipsum passages, and
more recently with desktop publishing software like Aldus PageMaker
including versions of Lorem Ipsum.

### This is some text

Lorium ipsum this is some great text \(\frac{\beta}{\gamma}\) with
some more text.  Then we add some display text. Some more stuff to
talk about.  is simply dummy text of the printing and typesetting
industry. Lorem Ipsum has been the industry's standard dummy text ever
since the 1500s, when an end of sentance.

| Option | Description |
| :----- | :---------- |
| data   | path to data files to supply the data that will be passed into templates. |
| engine | engine to be used for processing templates. Handlebars is the default. |
| ext    | extension to be used for dest files. |

This unknown printer took a galley of type and scrambled it to make a
type specimen book. It has survived not only five centuries, but also
the leap into electronic typesetting, remaining essentially
unchanged. It was popularised in the 1960s with the release of
Letraset sheets containing Lorem Ipsum passages, and more recently
with desktop publishing software like Aldus PageMaker including
versions of Lorem Ipsum.

\[\left(\frac{\partial P}{\partial V}\right) = -U\]

\[
 \frac{1}{\displaystyle 1+
    \frac{1}{\displaystyle 2+
       \frac{1}{\displaystyle 3+x}}} +
        \frac{1}{1+\frac{1}{2+\frac{1}{3+x}}}
\]

Yes that was supposed to a great text
[google](https://google.com). With some more text included. Lorium
ipsum this is some great text \(\frac{\beta}{\gamma}\) with some more
text.  Then we add some display text. Some more stuff to talk about.
is simply dummy text of the printing and typesetting industry. Lorem
Ipsum has been the industry's standard dummy text ever since the
1500s, when an unknown printer took a galley of type and scrambled it
to make a type specimen book. It has survived not only five centuries,
but also the leap into electronic typesetting, remaining essentially
unchanged. It was popularised in the 1960s with the release of
Letraset sheets containing Lorem Ipsum passages, and more recently
with desktop publishing software like Aldus PageMaker including
versions of Lorem Ipsum.

![Stranger Things](/images/stranger_things.png)

> a quote from the group that I would like to state

``` python
def foo(bar):
    a = 1 + 3
    print(bar)
    print("asdf")
    pass
```

Lorium ipsum this is some great text \((\frac{\beta}{\gamma + \omega})\) with
some more text.  Then we add some display text. Some more stuff to
talk about.  is simply dummy text of the printing and typesetting
industry. Lorem Ipsum has been the industry's standard dummy text ever
since the 1500s, when an unknown printer took a galley of type and
scrambled it to make a type specimen book. It has survived not only
five centuries, but also the leap into electronic typesetting,
remaining essentially unchanged. It was popularised in the 1960s with
the release of Letraset sheets containing Lorem Ipsum passages, and
more recently with desktop publishing software like Aldus PageMaker
including versions of Lorem Ipsum.

``` c
#include <stdio.h>

int main(int argc, char *argv[]) {
    int a = 1;
    a += 1;
    return 0;
}
```

> I've spent nearly 10 years coaching
> Facebook and Instagram and Twitter — on what
> kinds of news and photos I don’t want to see, and they all behaved
> accordingly. Each time I liked an article, or clicked on a link, or
> hid another, the algorithms that curate my streams took notice and
> showed me only what they thought I wanted to see. That meant I
> didn’t realize that most of my family members, who live in rural
> Virginia, were voicing their support for Trump online, and I didn’t
> see any of the pro-Trump memes that were in heavy circulation before
> the election. I never saw a Trump hat or a sign or a shirt in my
> feeds, and the only Election Day selfies I saw were of people
> declaring their support for Hillary Clinton.

Later some more text to show
