{
    "title": "Building a Polymer Static Site Generator",
    "author": "Chris Ostrouchov",
    "date": "2018-01-10",
	"image": "/images/blog/polymer_plus_static_site.png",
    "tags": ["polymer", "javascript"]
}

There are many static site generation frameworks out there such as
[Jekyll](https://jekyllrb.com/) and
[Hugo](https://gohugo.io/). Previously my site was written using
Jekyll but I often ran into limitations using the framework. I have
also tried using Hugo by helping a friend Alex set up a personal
[website](http://cv.walexhanson.com/). I have created a **compiling
free** static site generator that doesn't even deserve to be called a
framework. This is because the 60 line python script with no
dependencies is used to create a `robots.txt`, `sitemap.xml`, and
index the blog. That's it! The rest of the heavy lifting is done with
Polymer and a few custom elements that run on the website. All site
content is written in markdown just like traditional static site
generators but is instantly viewable. Feel free to view the
[code](https://gitlab.aves.io/costrouc/chrisostrouchov.com-polymer)

I **love** [Polymer](https://github.com/Polymer/polymer). I have
already created two websites ([material toolkit](https://mtk.aves.io),
[University of Tennessee eggdrop](https://eggdrop.science)) using Polymer and
wanted to finally use it to make my own personal site. This
requirement alone restricted using traditional static site generators
since they were not designed for single page app where each view does
not necessarily correspond to an html file.

Additionally I wanted to go one step futher and try to re-imagine what
a static site could look like when using web components. For instance,
I have `<code-highlight>`, `<markdown-it>`, `<google-analytics>` and
`<math-tex>` elements. These elements alone provide features that are
often hard to get working with a blog. Since all pages are markdown
files rendered with the custom element `<markdown-it>`, I have support
for code and math throughout the entire site. Also no compiling is
necessary to view the markdown files on the website!

**Adding a page** to the website is as simple as adding a markdown
file to the `page/` directory and adding the page to
`<app-route>`. For example, if you wanted a new page `about` with
route `chrisostrouchov.com/about` you would create `pages/about.md`
and modify `src/my-app.html` as such.

```html
<iron-pages selected="[[selectedPage]]"
            attr-for-selected="name"
            fallback-selection="404"
            role="main">
	 <div name="about" class="container">
         <markdown-it src="/pages/about.md"></markdown-it>
     </div>
     <!-- additional routes -->
</iron-pages>
```

**Making blog posts easy** was the main goal of this framework. Each blog
post is a markdown file placed in the `blog/` directory with some
additional `json` front matter. A detailed description on `json` front
matter can be found on the [Hugo front matter
documentation](https://gohugo.io/content-management/front-matter/). An
example of the front matter is shown bellow. The resulting blog post
url is `chrisostrouchov.com/blog/<filename>`.

```javascript
{
    "title": "Hello World Example",
    "author": "Chris Ostrouchov",
    "date": "2016-12-04",
    "image": "/images/blog/paul-gilmore.jpg",
    "tags": ["test", "hello world"]
}
```

To summarize I hope I've shown a neat way of simplifying static site
generators by not requiring them to generate the html pages
themselves. In theory any language could be used to create the
`robots.txt`, `sitemap.xml` and index the blog to an `index.json`
file. This is my first blog post but am hoping to do many more!
