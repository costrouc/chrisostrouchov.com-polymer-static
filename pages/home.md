My name is Chris Ostrouchov and I am a graduate student studying
Computational Material Science at the University of Tennessee,
Knoxville graduating around Spring 2017. I am fortunate to study under
my advisor
[Dr. William Weber](http://web.utk.edu/~mse/faculty/weber/default.html),
a governor's chair for MS&E.

My research revolves around High Performance Computing and its
applications in Radiation Material Science. Computing offers many
advantages in Radiation Material Science in that we can see the
fundamental physics taking place in a material at short time
scales. For example we can [see](https://youtu.be/kIbXBnGRV08) the
damage produced by an high energy ion. Currently I work heavily with
Density Functional Theory
([DFT](https://en.wikipedia.org/wiki/Density_functional_theory))
[VASP, Quantum Espresso] and Molecular Dynamics
([MD](https://en.wikipedia.org/wiki/Molecular_dynamics)) [LAMMPS] to
create MD potentials through Machine Learning.
