# Research

 - [Google Scholar](http://scholar.google.com/citations?user=bvSqVLsAAAAJ&hl=en)
 - [Research Id](http://www.researcherid.com/rid/B-2977-2014)

## Molecular Dynamics Studies of LiNbO3

LiNbO3 is a material that our group is very interested in that has
seen few MD studies due to it's complex triclinic crystal
structure. We are modeling the effects of high energy ions passing
through LiNbO3 by applying the thermal shock model. The above image is
a rendering of the electron density of LiNbO3 from a ground state
Quantum Espresso run.

## Monte Carlo Simulation of Ion Cascades

![Density Map of Ion Collisions](/images/position_NiCr_1000.png)

[SRIM](http://www.srim.org/) is a closed source code originally
written in the 1970's to calculate the range and stopping
power of ions in matter. The code has gone through periodic
updates with the most recent coming out in June 2013. Many
groups in Nuclear Engineering and Material Science at the
University of Tennessee use this software frequently and are
frustrated with its restrictions, speed, and ability to only
run on windows. Our group's aim is to provide an open source,
more stable, platform independent alternative to SRIM. See
Github for updates.

## Machine Learning and Molecular Dynamic Potentials

[DFTFIT](https://github.com/costrou/dftfit)

The creation of Molecular Dynamics potentials typically are
made through the use of fitting experimental data and Density
Functional Theory data. Currently there are limited tools
available online for creation of MD potentials. My goal for
this research is to tie in well established codes (VASP,
Quantum Espresso, and LAMMPS) for usage in constructing
molecular dynamics potentials. More information to come!
Please see Github for real-time updates.


## Publications

### Journal Publications

 - Zhang Y., Aidhy D., Varga T., Moll S., Edmondson P, Namavar P., Jin
   K., __Ostrouchov C.__, Weber W.,
   [The effect of electronic energy loss on irradiation-induced grain growth in nanocrystalline oxides](http://pubs.rsc.org/en/Content/ArticleLanding/2014/CP/c4cp00392f),
   Physical Chemistry Chemical Physics, Vol. 16, pg. 8051-8059,
   Feb. 2014  ([doi:10.1039/C4CP00392F](http://dx.doi.org/10.1039/C4CP00392F"))
 - Jones, A., __Ostrouchov, C.__, Harancyzk, M., Iglesia, E.,
   [From Rays to Structures: Void Structure Representations from Stochastic Calculations](http://www.sciencedirect.com/science/article/pii/S1387181113003697),
   Elsevier, Vol. 181, pg. 208-216, Nov. 2013,
   ([doi:10.1016/j.micromeso.2013.07.003](http://dx.doi.org/10.1016/j.micromeso.2013.07.033)
 - __Ostrouchov, C.__, Richardson, K.,
   [A Combined Numerical and Experimental Approach to Measuring Gap Conductance for Precision Glass Molding](http://www.mse.vt.edu/academicgroups/JUMR/tabid/669/Default.aspx),
   Journal of Undergraduate Materials Science, Oct. 2011

### Department Publications

 - __Ostrouchov, C.__, Haranczyk, M., Computational Tools
   for Geometry Based Analysis of Crystalline Porous Materials,
   NSF - Lawrence Berkeley National Lab - Research Report , Jul. 2012
 - __Ostrouchov, C.__, Wactel, P., Richardson, K., Examination of High
   Temperature Infrared Optical Materials in the GeAsS Ternary Glass
   System, NSF - International Research Experience for Undergraduates
   , July 2011
