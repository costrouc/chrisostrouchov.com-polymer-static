# Software

## [Zeo++](http://www.maciejharanczyk.info/Zeopp/)

![MFI Zeolite](/images/zeolite.png)

During the summer of 2012, I was given the opportunity of an
internship at Lawrence Berkeley National Lab (LBNL) working under
[Dr. Maciej Haranczyk](http://www.maciejharanczyk.info). I worked on
Zeo++ throughout that summer and added features to characterize the
distribution of the voids within a given Zeolite. This work
materialized into a paper published in early 2013. Zeo++ is written in
C++ with interfaces through python and
[VMD](http://www.ks.uiuc.edu/Research/vmd/). Zeo++ allows for the
calculation of geometrical parameters describing pores within the
material and easy visualization of these parameters.

## [PYQE](https://github.com/costrou/pyqe)

![LiNbO3](/images/density_alpha.png)

While taking MSE 613 (Introduction to Density Functional Theory) by
[Dr. Haixuan Xu](http://web.utk.edu/~mse/faculty/xu/default.html)we
made heavy use of Quantum Espresso. I was often frustrated by syntax
errors in my input scripts along with how many steps could not be
easily automated. BASH scripting was used at first but I quickly
learned that by creating a Python library I could speed up the
process. The software is open-source and available on
[Github](https://github.com/costrou/pyqe).


## [DFTFIT](https://github.com/costrou/dftfit)

DFTFIT is a software that uses DFT simulation data for fitting
molecular dynamic potentials. DFTFIT uses the least square method to
fit the stresses, total energy, and forces of a given set of
configurations. The user has the flexibility of changing the weights
for fitting and uses relative error to judge the fitness of
parameters. While this code is in its early stages, it has already
proven that it can come up with good potentials for MgO. After a
potential has been created, DFTFIT has tools for easily calculating
measurable experimental properties such as the lattice constant and
bulk modulus. The software is open-source and available on
[Github](https://github.com/costrou/dftfit).
