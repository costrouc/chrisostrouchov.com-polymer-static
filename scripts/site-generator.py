 #!/usr/env python
"""Simple static site generator

Polymer does all the heavy lifting
"""
import os


def create_robotstxt(base_url, base_path):
    ROBOTS_TEMPLATE = """
    User-agent: *
    Disallow:
    Sitemap: {base_url}/sitemap.xml
    """

    with open(os.path.join(base_path, 'robots.txt'), 'w') as f:
        f.write(ROBOTS_TEMPLATE.format(base_url=BASE_URL))


def create_sitemap(base_url, base_path):
    import time
    import xml.etree.cElementTree as ET

    def add_url(root, page_url, filename):
        url = ET.SubElement(root, 'url')
        ET.SubElement(url, 'loc').text = page_url
        time_struct = time.gmtime(os.path.getmtime(filename))
        ET.SubElement(url, 'lastmod').text = '%4d-%02d-%02d' % (
            time_struct.tm_year, time_struct.tm_mon, time_struct.tm_mday)

    root = ET.Element("urlset", xmlns="http://www.sitemaps.org/schemas/sitemap/0.9")
    for filename in os.listdir('blog'):
        add_url(root,
                '%s/blog/%s' % (base_url, filename[:filename.rfind('.')]),
                '%s/%s' % ('blog', filename))
    for filename in os.listdir('pages'):
        add_url(root,
                '%s/%s' % (base_url, filename[:filename.rfind('.')]),
                '%s/%s' % ('pages', filename))
    tree = ET.ElementTree(root)
    tree.write(os.path.join(base_path, 'sitemap.xml'),
               encoding='utf-8', xml_declaration=True)


def create_blog_index(base_path, description_length=256):
    import json

    blog_index = {}
    for filename in os.listdir('blog'):
        if filename == 'index.json':
            continue
        with open(os.path.join('blog', filename)) as f:
            content = f.read()
            json_end = content.find('}\n\n')
            data = json.loads(content[:json_end+1])
            blog_id = filename[:filename.rfind('.')]
            data['description'] = content[json_end+3:json_end+3+description_length] + '...'
            blog_index[blog_id] = data
    json.dump(
        blog_index,
        open(os.path.join(base_path, 'blog/index.json'), 'w'))


if __name__ == '__main__':
    BASE_URL = "https://chrisostrouchov.com"
    for base_path in ['./build/es5-bundled/', './']:
        create_robotstxt(BASE_URL, base_path)
        create_sitemap(BASE_URL, base_path)
        create_blog_index(base_path)
